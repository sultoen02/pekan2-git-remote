<?php

    require("animal.php");
    require("ape.php");
    require("frog.php");

    $sheep = new Animal("shaun");
    $sheep->hewan();

    echo "<br><br>";
    $kodok = new Frog("buduk");
    $kodok->hewan();
    $kodok->jump() ; // "hop hop"

    echo "<br><br>";
    $sungokong = new Ape("kera sakti");
    $sungokong->hewan();
    $sungokong->yell(); // "Auooo"
?>