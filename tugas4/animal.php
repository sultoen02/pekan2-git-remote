<?php

    class Animal{
        public $name;
        public $legs = 4;
        public $cold_blooded = "no";
        
        public function __construct($string){
            $this->name = $string;
        }

        public function hewan(){
            echo "Name : " . $this->name . "<br>"; // "shaun"
            echo "Legs : " . $this->legs . "<br>"; // 4
            echo "Cold Blooded : " . $this->cold_blooded . "<br>"; // "no"
        }
    }

?>